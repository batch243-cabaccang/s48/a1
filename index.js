const form = document.getElementById("form");
const title = document.getElementById("title");
const body = document.getElementById("body");
const post = document.getElementById("post");

const editForm = document.getElementById("edit-form");
const editId = document.getElementById("edit-id");
const editTitle = document.getElementById("edit-title");
const editBody = document.getElementById("edit-body");

const DUMMY_POSTS = [];
let count = 1;

const postsHandler = (id) => {
    let posts = "";
    // if (id) {
    //     for (let i = 0; i < DUMMY_POSTS.length; i++) {
    //         if (DUMMY_POSTS[i].id == id) {
    //             DUMMY_POSTS.splice(i, 1);
    //             break;
    //         }
    //     }
    // }
    DUMMY_POSTS.forEach((postEl) => {
        posts += `<div id="post-${postEl.id}">
        <h3 id="title-${postEl.id}">${postEl.title}</h3>
        <p id="body-${postEl.id}">${postEl.body}</p>
        <button id="update-btn" onclick="editHandler(${postEl.id})">Edit</button>
        <button id="update-btn" onclick="deleteHandler(${postEl.id})">Delete</button>
        </div>`;
    });
    post.innerHTML = posts;
};

// FOR ACTIVITY ------------------------------------------------------------
const deleteHandler = (id) => {
    for (let i = 0; i < DUMMY_POSTS.length; i++) {
        if (DUMMY_POSTS[i].id == id) {
            DUMMY_POSTS.splice(i, 1);
            break;
        }
    }
    document.getElementById(`post-${id}`).remove();
};
// --------------------------------------------------------------------------

const editHandler = (id) => {
    let title = document.getElementById(`title-${id}`).textContent;
    let body = document.getElementById(`body-${id}`).textContent;

    editTitle.value = title;
    editBody.value = body;
    editId.value = id;
};

const formHandler = (event) => {
    event.preventDefault();
    const post = {
        id: count,
        title: title.value,
        body: body.value,
    };

    DUMMY_POSTS.push(post);
    count++;
    postsHandler();
};

const editFormHander = (event, id) => {
    event.preventDefault();
    for (let i = 0; i < DUMMY_POSTS.length; i++) {
        if (DUMMY_POSTS[i].id == id) {
            DUMMY_POSTS[i].title = editTitle.value;
            DUMMY_POSTS[i].body = editBody.value;
            postsHandler();
            break;
        }
    }
};

form.addEventListener("submit", (event) => {
    formHandler(event);
});

editForm.addEventListener("submit", (event) => {
    editFormHander(event, editId.value);
});
